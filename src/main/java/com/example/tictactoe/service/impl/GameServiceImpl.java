package com.example.tictactoe.service.impl;

import com.example.tictactoe.Board;
import com.example.tictactoe.GameState;
import com.example.tictactoe.enums.GameStage;
import com.example.tictactoe.enums.Marker;
import com.example.tictactoe.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class GameServiceImpl implements GameService {
    private static final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);
    private static final String DRAW = "It's a draw!";
    private static final String WIN = " wins!";
    private static final String TURN = "Turn: ";
    private static final String X = "X";
    private static final String O = "0";

    //TODO refactor method
    @Override
    public void determineBestMove(GameState gameState) {
        Marker board[][] = gameState.getBoard().board;
        Marker playerMarker = gameState.getTurn();
        Marker opponentMarker = playerMarker.equals(Marker.X) ? Marker.O : Marker.X;

        if (board[1][1].equals(Marker.BLANK)) {
            if ((board[0][1].equals(opponentMarker) &&
                    board[2][1].equals(opponentMarker)) ||
                    (board[1][0].equals(opponentMarker) &&
                            board[1][2].equals(opponentMarker)) ||
                    (board[0][0].equals(opponentMarker) &&
                            board[2][2].equals(opponentMarker)) ||
                    (board[0][2].equals(opponentMarker) &&
                            board[2][0].equals(opponentMarker))) {

                try {
                    gameState.getBoard().move(1, 1, playerMarker);
                    return;
                } catch (Exception e) {
                }
            }
        }

        for (int r = 0; r < 3; ++r) {
            int bCount = 0;
            int oCount = 0;
            for (int c = 0; c < 3; ++c) {
                if (board[r][c].equals(opponentMarker)) {
                    ++oCount;
                }
                if (board[r][c].equals(Marker.BLANK)) {
                    ++bCount;
                }
            }

            if ((oCount == 2) && (bCount == 1)) {
                for (int c = 0; c < 3; ++c) {
                    if (board[r][c].equals(Marker.BLANK)) {
                        try {
                            gameState.getBoard().move(r, c, playerMarker);
                            return;
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }

        for (int c = 0; c < 3; ++c) {
            int bCount = 0;
            int oCount = 0;
            for (int r = 0; r < 3; ++r) {
                if (board[r][c].equals(opponentMarker)) {
                    ++oCount;
                }
                if (board[r][c].equals(Marker.BLANK)) {
                    ++bCount;
                }
            }

            if ((oCount == 2) && (bCount == 1)) {
                for (int r = 0; r < 3; ++r) {
                    if (board[r][c].equals(Marker.BLANK)) {
                        try {
                            gameState.getBoard().move(r, c, playerMarker);
                            return;
                        } catch (Exception e) {
                        }
                    }
                }
            }
        }

        int bCount = 0;
        int oCount = 0;
        int r = 0;
        int c = 0;
        for (int i = 0; i < 3; ++i) {
            if (board[r][c].equals(opponentMarker)) {
                ++oCount;
            }
            if (board[r][c].equals(Marker.BLANK)) {
                ++bCount;
            }
            ++r;
            ++c;
        }
        if ((oCount == 2) && (bCount == 1)) {
            r = 0;
            c = 0;
            for (int i = 0; i < 3; ++i) {
                if (board[r][c].equals(Marker.BLANK)) {
                    try {
                        gameState.getBoard().move(r, c, playerMarker);
                        return;
                    } catch (Exception e) {
                    }
                }
                ++r;
                ++c;
            }
        }
        r = 0;
        c = 2;
        bCount = 0;
        oCount = 0;
        for (int i = 0; i < 3; ++i) {
            if (board[r][c].equals(opponentMarker)) {
                ++oCount;
            }
            if (board[r][c].equals(Marker.BLANK)) {
                ++bCount;
            }
            ++r;
            --c;
        }
        if ((oCount == 2) && (bCount == 1)) {
            r = 0;
            c = 2;
            for (int i = 0; i < 3; ++i) {
                if (board[r][c].equals(Marker.BLANK)) {
                    try {
                        gameState.getBoard().move(r, c, playerMarker);
                        return;
                    } catch (Exception e) {
                        log.info("");
                    }
                }
                ++r;
                --c;
            }
        }

        if (board[1][1].equals(Marker.BLANK)) {
            try {
                gameState.getBoard().move(1, 1, playerMarker);
                return;
            } catch (Exception e) {
            }
        }

        boolean found = false;
        Random random = new Random();
        while (!found) {
            r = random.nextInt(3);
            c = random.nextInt(3);
            if (board[r][c].equals(Marker.BLANK)) {
                try {
                    gameState.getBoard().move(r, c, playerMarker);
                    found = true;
                } catch (Exception e) {
                    log.error("Problem making random move!", e);
                }
            }
        }
    }

    @Override
    public void evaluateBoard(GameState gameState) {
        Board board = gameState.getBoard();
        if (board.isDraw()) {
            gameState.setGameMessage(DRAW);
            gameState.setGameStage(GameStage.POST_GAME);
        } else if (board.isWinner(gameState.getTurn())) {
            if (gameState.getTurn().equals(Marker.O)) {
                gameState.setGameMessage(O + WIN);
            } else {
                gameState.setGameMessage(X + WIN);
            }
            gameState.setGameStage(GameStage.POST_GAME);
        } else {
            if (gameState.getTurn() == Marker.X) {
                gameState.setTurn(Marker.O);
                gameState.setTurnMessage(TURN + O);
            } else {
                gameState.setTurn(Marker.X);
                gameState.setTurnMessage(TURN + X);
            }
        }
    }
}

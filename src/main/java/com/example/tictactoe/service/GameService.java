package com.example.tictactoe.service;

import com.example.tictactoe.GameState;

public interface GameService {

    void determineBestMove(GameState gameState);

    void evaluateBoard(GameState gameState);
}

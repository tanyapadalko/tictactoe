package com.example.tictactoe;


import com.example.tictactoe.enums.Marker;

public class Board {

    public Marker[][] board = new Marker[3][3];


    public void clear() {
        for (int r = 0; r < 3; ++r) {
            for (int c = 0; c < 3; ++c) {
                board[r][c] = Marker.BLANK;
            }
        }
    }

    public String markAt(int row, int col) {
        Marker marker = board[row][col];
        if (marker.equals(Marker.X)) {
            return "X";
        } else if (marker.equals(Marker.O)) {
            return "O";
        } else if (marker.equals(Marker.BLANK)) {
            return " ";
        }
        return "#";
    }


    public void move(int row, int col, Marker marker) throws Exception {
        if (board[row][col] != Marker.BLANK) {
            throw new Exception("Square @ (" + row + ", " + col + ") is not empty");
        }
        if (marker == Marker.BLANK) {
            throw new IllegalArgumentException("Playing a BLANK marker is not valid");
        }
        board[row][col] = marker;
    }


    public boolean isWinner(Marker marker) {
        for (int r = 0; r < 3; ++r) {
            boolean isWinner = true;
            for (int c = 0; isWinner && (c < 3); ++c) {
                if (board[r][c] != marker) {
                    isWinner = false;
                }
            }
            if (isWinner) {
                return true;
            }
        }

        for (int c = 0; c < 3; ++c) {
            boolean isWinner = true;
            for (int r = 0; isWinner && (r < 3); ++r) {
                if (board[r][c] != marker) {
                    isWinner = false;
                }
            }
            if (isWinner) {
                return true;
            }
        }

        if ((board[0][0] == marker) && (board[1][1] == marker) && (board[2][2] == marker))
            return true;

        return (board[2][0] == marker) && (board[1][1] == marker) && (board[0][2] == marker);

    }


    public boolean isDraw() {
        for (int r = 0; r < 3; ++r) {
            for (int c = 0; c < 3; ++c) {
                if (board[r][c].equals(Marker.BLANK)) {
                    return false;
                }
            }
        }
        return true;
    }
}


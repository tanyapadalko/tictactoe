package com.example.tictactoe.controller;

import com.example.tictactoe.Board;
import com.example.tictactoe.GameState;
import com.example.tictactoe.enums.GameMode;
import com.example.tictactoe.enums.GameStage;
import com.example.tictactoe.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class GameController {
    private static final Logger log = LoggerFactory.getLogger(GameController.class);
    private static final String AI_MODE = "ai";
    private static final String PLAYERS_MODE = "twoplayer";

    @Autowired
    private GameService gameService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String game(HttpSession session, Model model) {
        GameState gameState = getStateFromSession(session);
        if (gameState == null) {
            gameState = new GameState();
            putStateInSession(session, gameState);
        }
        model.addAttribute("gameState", gameState);
        return "tictactoe";
    }

    @RequestMapping(value = "/tictactoe/reset", method = RequestMethod.GET)
    public String reset(HttpSession session, Model model) {
        GameState gameState = new GameState();
        putStateInSession(session, gameState);
        model.addAttribute("gameState", gameState);
        return "tictactoe";
    }

    @RequestMapping(value = "/tictactoe/new", method = RequestMethod.GET)
    public String gameNew(HttpSession session, Model model) {
        GameState gameState = getStateFromSession(session);
        gameState.startNewGame();
        model.addAttribute("gameState", gameState);
        return "tictactoe";
    }


    @RequestMapping(value = "/tictactoe/modeselection", method = RequestMethod.GET)
    public String modeSelected(HttpSession session, @RequestParam(value = "mode") String mode, Model model) {
        GameState gameState = getStateFromSession(session);
        switch (mode) {
            case AI_MODE:
                gameState.setGameMode(GameMode.AI_VS_HUMAN);
                break;
            case PLAYERS_MODE:
                gameState.setGameMode(GameMode.HUMAN_VS_HUMAN);
                break;
            default:
                throw new RuntimeException("Invalid selected game mode:" + mode);
        }
        model.addAttribute("gameState", gameState);
        return "redirect:/tictactoe/new";
    }


    @RequestMapping(value = "/tictactoe/move", method = RequestMethod.GET)
    public String playerMove(HttpSession session,
                             @RequestParam(value = "row") Integer row,
                             @RequestParam(value = "col") Integer col,
                             Model model) {

        GameState gameState = getStateFromSession(session);
        model.addAttribute("gameState", gameState);
        log.info("move=(" + row + ", " + col + ")");

        if (!gameState.getGameStage().equals(GameStage.IN_GAME)) {
            return "tictactoe";
        }
        Board board = gameState.getBoard();
        try {
            board.move(row, col, gameState.getTurn());
            gameService.evaluateBoard(gameState);

            if (gameState.getGameStage().equals(GameStage.IN_GAME) &&
                    gameState.getGameMode().equals(GameMode.AI_VS_HUMAN)) {
                gameService.determineBestMove(gameState);
                gameService.evaluateBoard(gameState);
            }
        } catch (Exception e) {
            log.error("Cannot complete move", e);
        }
        return "tictactoe";
    }


    private GameState getStateFromSession(HttpSession session) {
        GameState gameState = (GameState) session.getAttribute("gameState");
        if (gameState == null) {
            gameState = new GameState();
            putStateInSession(session, gameState);
        }
        return gameState;
    }


    private void putStateInSession(HttpSession session, GameState gameState) {
        session.setAttribute("gameState", gameState);
    }
}

package com.example.tictactoe.enums;

public enum Marker {

    BLANK, X, O
}

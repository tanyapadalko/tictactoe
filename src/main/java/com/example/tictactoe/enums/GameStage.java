package com.example.tictactoe.enums;

public enum GameStage {

    MODE_SELECTION,
    PLAYER_IDENTIFICATION,
    IN_GAME,
    POST_GAME
}
